exit; # this is just to show the workflow, it's not intended as a script

# tested on Ubunti 16 LTS
# tested with circos-0.69-5

# Other dependencies:   VarScan
#                       samtools
#                       bedtools

# you'll need:  aligned.bam
#               aligned.bam.bai
#               genome.fasta
#               genome.fasta.fai

# The mcov2circos and vcf2circos have only been tested on FALCON assemblies, you might need to tweak
# the regex for contig names if they don't play nice on other assemblies



# make the karyotype file
    # get gen size
GENSIZE=`cat genome.fasta.fai | awk '{ gen += $2 } END { print gen }'`

    # print the kar file "header"
printf "chr - p_ctgs p_ctgs 0 $GENSIZE white\n" > genome.kar.txt

    # add in the contigs as bands, tweak the sed regex as needed - edit/remove sed regex if needed
cat genome.fasta.fai | sort -k2,2nr | sed 's/|quiver\||arrow//g' | perl -e 'my $t=0; my $c="white"; while(my @l = split(/\s+/, <>)){my $e=$t+$l[1]; print STDOUT "band p_ctgs $l[0] $l[0] $t $e $c\n"; if ($c eq "white"){$c="black"}else{$c="white"}; $t+=$l[1];}' >> genome.kar.txt


# make the bed windows (this example is for 100kb windows and 50kb steps, choose something suitable for your genome/contig size)
    # a window size of approx 1/1000th the size of the genome seems to work well
bedtools makewindows -g genome.fasta.fai -w 100000 -s 50000 > genome.windows.bed


# get the reads per window
bedtools multicov -bams aligned.bam -bed genome.windows.bed > aligned.mcov

# convert it to circos histogram format
mcov2circos.pl -b aligned.mcov -k genome.kar.txt > coverage.hist.txt


# get some SNP calls
samtools mpileup -f genome.fasta aligned.bam | java -jar ../varscan.jar mpileup2snp --p-value 0.001 | gzip - > aligned.SNPs.tsv.gz

# filter for het snps only, and filter out snps from bad read depths (in this example between 20 and 200, adjust as needed)
zcat aligned.SNPs.tsv.gz | perl -e 'while(<>){my @l = split(/\s+/,$_); my @d = split(/:/,$l[4]); ($l[7] == 1) && ($d[1] > 20) && ($d[1] < 200) && (print $_);}' \
    | gzip - > aligned.SNPs.filt.tsv.gz

# convert to circos histogram format, the script should work with either vcf or tsv but it's very simple and wont recognise FILTER tags etc,
    # so filtered SNPs need to be completely removed from the file
vcf2circos.pl -v aligned.SNPs.filt.tsv.gz -b genome.windows.bed -k genome.kar.txt > snps.hist.txt


# then you'll need to modify the circos.conf and ticks.conf files with your file names, and modify the histogram threasholds etc to suit your data
    # the included example is the arabidopsis plot
circos -conf circos.conf

