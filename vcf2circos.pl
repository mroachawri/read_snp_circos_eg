#!/usr/bin/env perl
use strict;
use warnings;
use Getopt::Long;
use Data::Dumper;

#    Script for converting varscan data for use with circos
#    Copyright (C) 2017 Michael Roach
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

my $usage = "
USAGE:
vcf2circos_hist.pl  -v snps.vcf.gz  -b windows.bed  -k assembly.kar.txt
";

my $vcf;
my $kar;
my $bed;

my $VFH;
my $KFH;
my $BFH;

GetOptions(
    "vcf=s" => \$vcf,
    "kar=s" => \$kar, 
    "bed=s" => \$bed
) or die $usage;


if ( !(-s $vcf) || !(-s $kar) || !(-s $bed) ){
    die $usage;
}


open $KFH, $kar or die "failed to open $kar";
if ($vcf =~ /\.gz$/){
    open $VFH, "gunzip -c $vcf |" or die "failed to open pipe for $vcf\n";
} else {
    open $VFH, $vcf or die "failed to open $vcf";
}
open $BFH, $bed or die "failed to open $bed";


my %ctgs;           # $ctg{contig} = offset
my %windows;        # $windows{contig} = array of packed ranged, e.g. (0:100000, 50000:150000, ...)
my %snps;           # $snps{contig} = contig positions, e.g. (53500, 53513, 54217, ...)


# get contig lens
while(<$KFH>){
    $_ =~ s/\n//;
    if ($_){
        my @l = split (/\s+/, $_);
        $ctgs{$l[3]} = $l[4];
    }
}
close $KFH;


# parse the snp calls
while(<$VFH>){
    next if ($_ =~ /^#/);
    my @l = split(/\s+/, $_);
    next if ($l[1] !~ /\d/);
    $l[0] =~ s/\|quiver|\|arrow//g;    ##### This should remove the "|quiver|arrow" from the contig names, edit if needed
    push @{$snps{$l[0]}}, $l[1];
}
close $VFH;


# read in the bed windows
while(<$BFH>){
    $_ =~ s/\n//;
    if ($_){
        my @l = split(/\s+/, $_);
        $l[0] =~ s/\|quiver|\|arrow//g;    ##### This should remove the "|quiver|arrow" from the contig names, edit if needed
        push @{$windows{$l[0]}}, "$l[1]:$l[2]";
    }
}
close $BFH;


# iterate contig -> bed windows -> snps
foreach my $contig (keys(%ctgs)){
    foreach my $wind (@{$windows{$contig}}){
        my ($start, $end) = split(/:/, $wind);
        my $snp_count=0;
        foreach my $pos (@{$snps{$contig}}){
            next if ($pos < $start);
            last if ($pos > $end);
            $snp_count++;
        }
        my $ws = $start + $ctgs{$contig};
        my $we = $end + $ctgs{$contig};
        my $sc = -1000*(($snp_count + 1e-20) / ($we - $ws));
        print STDOUT "p_ctgs $ws $we $sc\n";
    }
}





