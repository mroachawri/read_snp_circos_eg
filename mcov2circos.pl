#!/usr/bin/env perl
use strict;
use warnings;
use Getopt::Long;

#    Script for converting bedtools data for use with circos
#    Copyright (C) 2017 Michael Roach
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

my $usage = "
USAGE:
vcf2circos_hist.pl  -b bam.mcov  -k assembly.kar.txt 
";

my $bam;
my $kar;

my $BFH;
my $KFH;


GetOptions(
    "bam=s" => \$bam,
    "kar=s" => \$kar
) or die $usage;


if ( !(-s $bam) || !(-s $kar) ){
    die $usage;
}


open $KFH, "<", $kar or die "failed to open $kar";
open $BFH, "<", $bam or die "failed to open $bam";


my %ctgs;   # $ctg{contig} = len

# get contig lens
while(<$KFH>){
    next if ($_ =~ /^chr/);
    my @l = split(/\s+/,$_);
    $ctgs{$l[3]}=$l[4];
}

close $KFH;


# convert coords
while(<$BFH>){
    $_=~s/\n//;
    if ($_){
        my @l=split(/\s+/,$_);
        $l[0] =~ s/\|quiver|\|arrow//g;    ##### This should remove the "|quiver|arrow" and everything after it from the contig names, edit if needed
        $l[1]+=$ctgs{$l[0]};
        $l[2]+=$ctgs{$l[0]};
        $l[3] = $l[3] / ($l[2]-$l[1]);
        print STDOUT "p_ctgs $l[1] $l[2] $l[3]\n";
    }
}











